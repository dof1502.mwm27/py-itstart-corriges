
heroes = [
    "Batman",
    "Deadpool",
    "Flash",
    "Les Gardiens de la Galaxie",
    "Wonder Woman",
    "Iron Man",
    "Captain Marvel",
    "Ant-Man",
    "Captain America",
    "Black Panther",
    "Hulk",
    "Elektra",
    "Blade",
    "Supergirl",
    "Thor",
    "Superman",
    "Nick Fury",
    "The Punisher",
    "Aquaman",
    "L'Homme-Chose",
    "Kick-Ass",
    "Ghost Rider",
    "Doctor Strange",
    "Suicide Squad",
    "Black Widow",
    "Spider-Man",
    "Daredevil",
    "Hawkeyes",
    "Les Quatre Fantastiques",
]

for superhero in heroes:
    if "man" in superhero or "Man" in superhero:
        print(superhero)

