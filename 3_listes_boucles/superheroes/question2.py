heroes = [
    "Batman",
    "Deadpool",
    "Flash",
    "Les Gardiens de la Galaxie",
    "Wonder Woman",
    "Iron Man",
    "Captain Marvel",
    "Ant-Man",
    "Captain America",
    "Black Panther",
    "Hulk",
    "Elektra",
    "Blade",
    "Supergirl",
    "Thor",
    "Superman",
    "Nick Fury",
    "The Punisher",
    "Aquaman",
    "L'Homme-Chose",
    "Kick-Ass",
    "Ghost Rider",
    "Doctor Strange",
    "Suicide Squad",
    "Black Widow",
    "Spider-Man",
    "Daredevil",
    "Hawkeyes",
    "Les Quatre Fantastiques",
]

print(heroes)
sorted_heros = sorted(heroes)
print(heroes)
print(sorted_heros)

print(heroes)
sort_result = heroes.sort()
print(heroes)
print(sort_result)
