
# On déclare la variable result. A la fin de l'algorithme
# elle contiendra le resultat final, c'est à dire chacune
# des lignes composant le triangle de pascal
result = [
    [1],
    [1, 1],
]

# Les 2 premières lignes de notre triangle sont déjà définies,
# on va donc commencer à travailler sur la 3e ligne, c'est à dire la ligne n°2
rang = 2

# La procédure suivante va s'executer TANT QUE rand est inférieur ou égal à 15
# Comme on incrémente rang à la fin de la boucle,
while rang <= 12:
    # On crée une nouvelle ligne, que l'on ajoutera plus tard à notre résultat
    nouvelle_ligne = []
    # On définni la variable ligne_prec afin de pouvoir lire les valeurs contenues dans la ligne précédente (rang - 1)
    ligne_prec = result[rang - 1]

    # C'est la partie un peu plus compliquée.
    # Cette boucle imbriquée va parcourir les éléments contenus dans ligne_prec un à un...
    i = 0
    while i < len(ligne_prec) - 1:
        # Dans la ligne précédente, on extrait chaque élément ligne_prec[i] et son suivant ligne_prec[i + 1]
        # On calcule la somme de ces deux valeurs
        somme = ligne_prec[i] + ligne_prec[i + 1]
        # et on enregistre cette somme dans notre nouvelle ligne
        nouvelle_ligne.append(somme)
        # On n'oublie pas d'incrémenter
        i += 1

    # On ajoute le 1er et le dernier 1 à la ligne courante
    nouvelle_ligne.insert(0, 1)
    nouvelle_ligne.append(1)

    # Et enfin, on ajoute notre ligne dans le résultat
    result.append(nouvelle_ligne)

    # On passe au rang suivant pour le prochain tour de boucle
    rang += 1


for ligne in result:
    print(ligne)
