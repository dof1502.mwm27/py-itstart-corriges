"""
Dans une classe avec un nombre d'élève inconnu, chaque élève possède 1 note. On souhaite connaitre la meilleure,
la moins bonne, et la moyenne de l'ensemble des notes.

**Écrivez un algorithme qui demande à l'utilisateur combien d'élèves sont présents, puis qui demande la note pour
chaque élève. Enfin vous afficherez le texte suivant :**

> la meilleure note est ... /20
> la plus mauvaise note est ... /20
> la moyenne de classe est ... /20
"""

N = int(input("Combien de notes ? "))
notes = []

while len(notes) < N:
    note = float(input("Merci de taper une note : "))
    notes.append(note)

meilleure = max(notes)
pire = min(notes)
moyenne = sum(notes) / len(notes)

print("La meilleure note est :", meilleure)
print("La pire note est :", pire)
print("La moyenne est de :", round(moyenne, 2))
