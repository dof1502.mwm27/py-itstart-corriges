"""
Dans une classe avec un nombre d'élève inconnu,
chaque élève possède 1 note. On souhaite connaitre
la meilleure,
la moins bonne, et la moyenne de l'ensemble des notes.

**Écrivez un algorithme qui demande à l'utilisateur combien d'élèves sont présents, puis qui demande la note pour
chaque élève. Enfin vous afficherez le texte suivant :**

> la meilleure note est ... /20
> la plus mauvaise note est ... /20
> la moyenne de classe est ... /20
"""

nb_notes = int(input("Combien d'eleves ? "))

total = 0
meilleure = -1
pire = 9999999

compteur = 0
while compteur < nb_notes:
    note = float(input("Merci de taper une note : "))

    total = total + note
    if note > meilleure:
        meilleure = note
    if note < pire:
        pire = note

    compteur = compteur + 1

moyenne = total / nb_notes
print("La meilleure note est", meilleure, "/20")
print("La pire note est", pire, "/20")
print("La moyenne de classe est", round(moyenne, 2), "/20")
