"""

Un magasin de reprographie facture dégressivement en fonction du nombre de photocopies effectuées :

- 0,10 € / photocopie pour les 10 premières
- 0,09 € / photocopie pour les 20 suivantes
- 0,08 € / photocopie au-delà.

**Écrivez un algorithme qui demande à l’utilisateur le nombre de photocopies effectuées
et qui affiche la facture correspondante.**

"""

nb_photocopies = int(input("Combien de photocopies avez-vous fait ? "))
n = nb_photocopies
total = 0

while n > 30:
    total = total + 0.08
    n -= 1
while n > 10:
    total += 0.09
    n -= 1
while n > 0:
    total += 0.10
    n -= 1

print("Vous avez fait", nb_photocopies, "photocopies, vous nous devez",
      round(total, 2), "€")
