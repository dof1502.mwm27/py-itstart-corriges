
def nb_fois_divisible_par_2(x):
    nb_total = 0
    while x % 2 == 0:
        nb_total += 1
        x /= 2
    return nb_total


# Vérification du résultat donné par la fonction pour x = ...:
valeurs_de_x = [5, 10, 62, 128, 1652, 1358]
for val_x in valeurs_de_x:
    print(val_x, "peut être divisé par 2", nb_fois_divisible_par_2(val_x), "fois")