def distance_A_B(latA, longA, latB, longB):
    """
    Cette fonction prend en paramètre les latitudes et longitudes de deux points de la Terre
    puis calcule la distance séparant ces deux points.
    Le résultat est exprimé en Km
    :param latA: Latitude du point A
    :param longA: Longitude du point A
    :param latB: Latitude du point B
    :param longB: Longitude du point B
    :return: La distance entre les points A et B, en Km
    """
    # On importe quelques fonctions mathématiques de Python
    from math import cos, sin, radians, acos

    # 🠗 Le rayon de la Terre 🠗 (si, si)
    RAYON_TERRE = 6378.137

    # On effectue quelques conversions degrés vers radians
    x1_rad = radians(latA)
    x2_rad = radians(latB)

    # On fait un calcul bien trop compliqué, qu'on est allé pomper sur un site internet à peu près fiable
    tmp_calcul = sin(x1_rad) * sin(x2_rad) + cos(x1_rad) * cos(x2_rad) * cos(radians(longB - longA))
    distance_km = acos(tmp_calcul) * RAYON_TERRE

    return distance_km


# Les coordonnées de Angers :
angers_lat = 47.471302
angers_long = -0.551753

# Lat, Long
coords = {
    'laval': {'lat': 48.078515, 'long': -0.7669906},
    'orléans': {'lat': 47.902964, 'long': 1.909251},
    'tours': {'lat': 47.394487, 'long': 0.685819},
    'nantes': {'lat': 47.218371, 'long': -1.553621},
    'caen': {'lat': 49.182863, 'long': -0.3706789},
    'le mans': {'lat': 48.006110, 'long': 0.1995560},
    'poitiers': {'lat': 46.580224, 'long': 0.340375},
    'rennes': {'lat': 48.117266, 'long': -1.6777926},
}

TARIF_PAR_KM = 600 / 100
ARGENT_EN_POCHE = 900
# Variables pour question2
max_distance = -1
ville_la_plus_loin = ""

for ville_arrivee in coords:
    coord_ville = coords[ville_arrivee]
    distance_from_angers = distance_A_B(angers_lat, angers_long, coord_ville['lat'], coord_ville['long'])
    tarif = TARIF_PAR_KM * distance_from_angers

    if tarif <= ARGENT_EN_POCHE:
        print("Je peux aller à", ville_arrivee, "pour la modique somme de", round(tarif, 2), "€")
        if max_distance < distance_from_angers:
            max_distance = distance_from_angers
            ville_la_plus_loin = ville_arrivee

# Question2
print("La ville atteignable la plus loin est:", ville_la_plus_loin, "elle est à une distance de", max_distance)
