

def est_multiple_de_7(x):
    if x != 0:
        if x % 7 == 0:
            result = True
        else:
            result = False
    else:
        result = False

    return result


print("Pour 0:", est_multiple_de_7(0))
print("Pour -5:", est_multiple_de_7(-5))
print("Pour 7:", est_multiple_de_7(7))
print("Pour 8:", est_multiple_de_7(8))
print("Pour 48:", est_multiple_de_7(48))
print("Pour 49:", est_multiple_de_7(49))
print("Pour -154:", est_multiple_de_7(-154))


