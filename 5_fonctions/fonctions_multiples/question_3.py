
def est_multiple(x, y):
    if x != 0:
        return x % y == 0
    else:
        return False


for x in range (1000):
    if est_multiple(x, 7):
        print(x, "est multiple de 7")
