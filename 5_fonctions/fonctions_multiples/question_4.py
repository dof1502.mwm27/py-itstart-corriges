
def est_multiple(x, y):
    if x != 0:
        return x % y == 0
    else:
        return False


for x in range (1000):
    if est_multiple(x, 54):
        print(x, "est multiple de 54")

print("--------------------------")

for x in range (1000):
    if est_multiple(x, 244):
        print(x, "est multiple de 244")

print("--------------------------")

for x in range (1000):
    if est_multiple(x, 66):
        print(x, "est multiple de 66")

