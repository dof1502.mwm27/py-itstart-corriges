
def est_multiple_de_7(x):
    if x != 0:
        if x % 7 == 0:
            result = True
        else:
            result = False
    else:
        result = False

    return result


for i in range(1000):

    if est_multiple_de_7(i):
        print(i, "est multiple de 7")
