
MINUTE = 60
HEURE = 60 * MINUTE
JOURNEE = 24 * HEURE


def secondes_vers_jours_heures_minutes(total_secondes):
    n_jours = total_secondes // JOURNEE
    sec_restantes = total_secondes % JOURNEE

    n_heures = sec_restantes // HEURE
    sec_restantes = sec_restantes % HEURE

    n_min = sec_restantes // MINUTE
    n_sec = sec_restantes % MINUTE

    print(total_secondes, "correspond à", n_jours, "jours,", n_heures,
        "heures,", n_min, "minutes et", n_sec, "secondes")


print(secondes_vers_jours_heures_minutes(3599))







# secondes_vers_jours_heures_minutes(123456789)
# secondes_vers_jours_heures_minutes(987654321)
# secondes_vers_jours_heures_minutes(93784)
# print("---------------------")
# secondes_vers_jours_heures_minutes(123456789000)
# secondes_vers_jours_heures_minutes(223456789000)
# secondes_vers_jours_heures_minutes(323456789000)
# secondes_vers_jours_heures_minutes(523456789000)
