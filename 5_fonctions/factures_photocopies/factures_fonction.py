
def facture_photocop(nb_photoc):

    if nb_photoc <= 10:
        total = 0.10 * nb_photoc
    elif 10 <= nb_photoc <= 30:
        total = 0.10 * 10 + (nb_photoc-10) * 0.09
    else:
        total = 0.10 * 10 + 0.09 * 20 + (nb_photoc - 30) * 0.08

    return total

def convertion(somme_euro):
    somme_dollar = somme_euro * 1.15
    return somme_dollar

facture_euro = facture_photocop(29)
facture_dollar = convertion(facture_euro)
print("Vous nous devez", round(facture_dollar, 2), "$")

