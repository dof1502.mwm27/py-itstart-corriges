
def est_premier(x):
    if x == 0 or x == 1:
        return False
    for n in range(2, x):
        if x % n == 0:
            return False
    return True


for x in range (100000):
    if est_premier(x):
        print(x)
