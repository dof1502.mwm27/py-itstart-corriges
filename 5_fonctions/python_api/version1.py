

def itstart_len(une_liste):
    total = 0
    for _ in une_liste:
        total += 1
    return total


def itstart_sum(une_autre_liste):
    total = 0
    for x in une_autre_liste:
        total += x
    return total


def itstart_max(encore_une_autre_liste):
    if len(encore_une_autre_liste) > 0:
        le_maximum = encore_une_autre_liste[0]
    else:
        le_maximum = 0

    for x in encore_une_autre_liste:
        if le_maximum < x:
            le_maximum = x
    return le_maximum


def itstart_range(borne_max):
    i = 0
    resultat = []
    while i < borne_max:
        resultat.append(i)
        i += 1
    return resultat

print(itstart_range(12))

# Une liste de 6 nombres premiers
premiers = [2, 3, 5, 7, 11, 13]
# Les 10 premiers multiples de 7
multiples_de_7 = [7, 14, 21, 28, 35, 42, 49, 56, 63, 70]

print("Taille de 'premiers' :", itstart_len(premiers))
print("Taille de 'multiples_de_7' :", itstart_len(multiples_de_7))
print("Max de 'premiers' :", itstart_max(premiers))
print("Max de 'multiples_de_7' :", itstart_max(multiples_de_7))
print("Somme de 'premiers' :", itstart_sum(premiers))
print("Somme de 'multiples_de_7' :", itstart_sum(multiples_de_7))

somme_des_100_premiers_entiers = itstart_sum(itstart_range(1001))
print("Somme des 1000 premiers entiers : ", somme_des_100_premiers_entiers)

