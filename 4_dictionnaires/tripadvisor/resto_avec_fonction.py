"""

================== Trip Advisor ========================
Vous êtes à la tête d'un site d'avis sur des restaurants.
Grâce aux informations laissées par vos visiteurs, vous disposez
pour chaque restaurant de 3 notes : une pour la qualité de la nourriture,
une pour la déco et une dernière pour le service.

Vous souhaitez simplifier la notation, pour cela, vous allez calculer pour
chaque restaurant la moyenne des 3 notes qui lui sont attribuées.
Affichez la moyenne de chaque restaurant.

Enfin Affichez une dernière ligne qui mettra en évidence le restaurant ayant la meilleure note moyenne.

"""

notes = {
    "Le Lion d'Or": {
        "nourriture": 12,
        "deco": 16,
        "service": 11,
    },

    "Pizza du Roy": {
        "nourriture": 18,
        "deco": 12.5,
        "service": 16,
    },

    "Le café du port": {
        "nourriture": 17,
        "deco": 13,
        "service": 14.5,
    },

    "Le balto": {
        "nourriture": 8,
        "deco": 11,
        "service": 19,
    },

    "O'tacos": {
        "nourriture": 13.5,
        "deco": 14,
        "service": 9,
    },
}


def moyenne(a, b, c):
    return (a + b + c) / 3


def moyenne_alt(a, b, c):
    somme = a + b + c
    mean = somme / 3
    return mean


for nom_resto in notes:
    resto_notes = notes[nom_resto]

    note_nourriture = resto_notes['nourriture']
    note_deco = resto_notes["deco"]
    note_service = resto_notes["service"]

    moy = moyenne(note_nourriture, note_deco, note_service)

    print("Moyenne pour", nom_resto, ":", round(moy, 2))



