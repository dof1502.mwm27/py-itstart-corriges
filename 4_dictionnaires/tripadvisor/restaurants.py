"""

================== Trip Advisor ========================
Vous êtes à la tête d'un site d'avis sur des restaurants.
Grâce aux informations laissées par vos visiteurs, vous disposez
pour chaque restaurant de 3 notes : une pour la qualité de la nourriture,
une pour la déco et une dernière pour le service.

Vous souhaitez simplifier la notation, pour cela, vous allez calculer pour
chaque restaurant la moyenne des 3 notes qui lui sont attribuées.
Affichez la moyenne de chaque restaurant.

Enfin Affichez une dernière ligne qui mettra en évidence le restaurant ayant la meilleure note moyenne.

"""

notes = {
    "Le Lion d'Or": {
        "nourriture": 12,
        "deco": 16,
        "service": 11,
    },

    "Pizza du Roy": {
        "nourriture": 18,
        "deco": 12.5,
        "service": 16,
    },

    "Le café du port": {
        "nourriture": 17,
        "deco": 13,
        "service": 14.5,
    },

    "Le balto": {
        "nourriture": 8,
        "deco": 11,
        "service": 19,
    },

    "O'tacos": {
        "nourriture": 13.5,
        "deco": 14,
        "service": 9,
    },
}

best_resto, best_note = "", -1

for resto in notes:

    dict_notes = notes[resto]

    moyenne = (dict_notes['nourriture'] + dict_notes["deco"] + dict_notes["service"]) / 3
    moyenne_arrondie = round(moyenne, 2)
    print("Moyenne pour", resto, ":", moyenne_arrondie)

    if moyenne > best_note:
        best_resto, best_note = resto, moyenne_arrondie

print("Et le gagnant est", best_resto, "avec une note de", best_note)
