
calendrier = {
    'janvier': 31,
    'février': 28,
    'mars': 31,
    'avril': 30,
    'mai': 31,
    'juin': 30,
    'juillet': 31,
    'aout': 31,
    'septembre': 30,
    'octobre': 31,
    'novembre': 30,
    'décembre': 31,
}
print("Il y a",
      calendrier["février"], "jours en février, ",
      calendrier["juillet"], "jours en juillet et",
      calendrier["décembre"], "jours au mois de décembre.")