
calendrier = {
    'janvier': 31,
    'février': 28,
    'mars': 31,
    'avril': 30,
    'mai': 31,
    'juin': 30,
    'juillet': 31,
    'aout': 31,
    'septembre': 30,
    'octobre': 31,
    'novembre': 30,
    'décembre': 31,
}

mois_30 = []
mois_31 = []

for mois in calendrier:

    nb_jours = calendrier[mois]
    if nb_jours == 31:
        mois_31.append(mois)

    elif nb_jours == 30:
        mois_30.append(mois)

print("30 jours:", mois_30)
print("31 jours:", mois_31)
