
calendrier = {
    'janvier': 31,
    'février': 28,
    'mars': 31,
    'avril': 30,
    'mai': 31,
    'juin': 30,
    'juillet': 31,
    'aout': 31,
    'septembre': 30,
    'octobre': 31,
    'novembre': 30,
    'décembre': 31,
}
# for mois in calendrier:
#     print(mois, "a pour valeur", calendrier[mois])
#
# print("--------------------")
#
# for mois, nbr_jours in calendrier.items():
#     print(mois, "compte", nbr_jours, "jours")
#
# print("--------------------")
total = 0
for mois, nbr_jours in calendrier.items():
    total += nbr_jours

print("Une année compte", total, "jours")
