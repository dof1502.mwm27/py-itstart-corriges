
sequence = "ACTGCTAATAAGGTCTAGCATAATTATGCTCTGAGATTCGCTCTAGGAGAGTCTCCTGGAAACGGTTAGAGGTCTCTCTGGAAAGGTCACATTAACCGATAGATTAGAGATCTCTCTGATAGAGGATCCTGTAATCTCGGGTGTATGACGATACAGCTGTCTGAC"

result = {}
for letter in sequence:
    if letter in result:
        result[letter] = result[letter] + 1
    else:
        result[letter] = 1

print(result)
