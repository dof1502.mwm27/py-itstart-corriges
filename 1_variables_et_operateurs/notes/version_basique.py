"""
Dans une classe de 4 élèves, chaque élève possède 1 note. On souhaite connaitre la meilleure, la moins bonne,
et la moyenne des 4 notes
**Écrivez un algorithme qui demande à l'utilisateur les 4 notes et qui affichera ceci :**

> la meilleure note est ... /20
> la plus mauvaise note est ... /20
> la moyenne de classe est ... /20
"""

note_1 = input('note 1:')
note_2 = input('note 2:')
note_3 = input('note 3:')
note_4 = input('note 4:')

intNote_1 = int(note_1)
intNote_2 = int(note_2)
intNote_3 = int(note_3)
intNote_4 = int(note_4)

max_note = max(intNote_1, intNote_2, intNote_3, intNote_4)
min_note = min(intNote_1, intNote_2, intNote_3, intNote_4)
moy_note = (intNote_1 + intNote_2 + intNote_3 + intNote_4) / 4

print('La meilleur note est: ', max_note, '/20')
print('La moins bonne note est: ', min_note, '/20')
print('La moyenne est: ', moy_note, '/20')



