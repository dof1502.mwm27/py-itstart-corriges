"""
Dans une classe de 4 élèves, chaque élève possède 1 note. On souhaite connaitre la meilleure, la moins bonne,
et la moyenne des 4 notes
**Écrivez un algorithme qui demande à l'utilisateur les 4 notes et qui affichera ceci :**

> la meilleure note est ... /20
> la plus mauvaise note est ... /20
> la moyenne de classe est ... /20
"""

note_1 = int(input("entrez une note"))
note_2 = int(input("entrez une note"))
note_3 = int(input("entrez une note"))
note_4 = int(input("entrez une note"))

moyenne = (note_1 + note_2 + note_3 + note_4) / 4
print("La moyenne de classe est de", moyenne, "/20.")

if note_1 > note_2 and note_1 > note_3 and note_1 > note_4:
    print(note_1, "/20 est la meilleure note.")
elif note_1 < note_2 and note_1 < note_3 and note_1 < note_4:
    print(note_1, "/20 est la pire note.")

if note_2 > note_1 and note_2 > note_3 and note_2 > note_4:
    print(note_2, "/20 est la meilleure note.")
elif note_2 < note_1 and note_2 < note_3 and note_2 < note_4:
    print(note_2, "/20 est la pire note.")

if note_3 > note_1 and note_3 > note_2 and note_3 > note_4:
    print(note_3, "/20 est la meilleure note.")
elif note_3 < note_1 and note_3 < note_2 and note_3 < note_4:
    print(note_3, "/20 est la pire note.")
    
if note_4 > note_1 and note_4 > note_2 and note_4 > note_3:
    print(note_4, "/20 est la meilleure note.")
elif note_4 < note_1 and note_4 < note_2 and note_4 < note_3:
    print(note_4, "/20 est la pire note.")
