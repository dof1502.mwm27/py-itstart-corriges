"""

============= L'heure système ==================================

L'heure système est exprimée en seconde depuis le début de l'ère
Informatique en 1970 environ. C'est le timestamp.
Écrivez un algorithme qui demandera un nombre entier positif
(Nombre de secondes) et qui l'affichera de la manière suivante :
X secondes correspond à X jours, X heures, X minutes et X secondes

"""
total_secondes = int(input("Entrez un timestamp (en secondes) : "))

MINUTE = 60
HEURE = 60 * MINUTE
JOURNEE = 24 * HEURE

n_jours = total_secondes // JOURNEE
sec_restantes = total_secondes % JOURNEE

n_heures = sec_restantes // HEURE
sec_restantes = sec_restantes % HEURE

n_min = sec_restantes // MINUTE
n_sec = sec_restantes % MINUTE

print(total_secondes, "correspond à", n_jours, "jours,", n_heures,
      "heures,", n_min, "minutes et", n_sec, "secondes")
