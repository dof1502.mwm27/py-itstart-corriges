"""

============= L'heure système ==================================

L'heure système est exprimée en seconde depuis le début de l'ère
Informatique en 1970 environ. C'est le timestamp.
Écrivez un algorithme qui demandera un nombre entier positif
(Nombre de secondes) et qui l'affichera de la manière suivante :
X secondes correspond à X jours, X heures, X minutes et X secondes

"""
valSeconde = int(input("Entrez un nombre de secondes ?"))
valSecondeBis = valSeconde

jours = 0
heure = 0
minutes = 0

while valSeconde >= 86400:
    jours = jours + 1
    valSeconde = valSeconde - 86400

while valSeconde >= 3600:
    heure = heure + 1
    valSeconde = valSeconde - 3600

while valSeconde >= 60:
    minutes = minutes + 1
    valSeconde = valSeconde - 60

print(valSecondeBis, "correspond à ", jours, " jours , ",
      heure, " heures , ", minutes, " minutes , ",
      valSeconde, " secondes !")
