
def volume_sphere(rayon):
    PI = 3.14159
    volume = ((4 / 3) * PI) * rayon ** 3
    return round(volume, 2)


resultat = volume_sphere(12)
print(resultat)
print(volume_sphere(20))
print(volume_sphere(40.1))
