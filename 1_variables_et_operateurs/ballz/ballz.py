"""
Écrivez un programme qui permet de calculer le volume d'une sphère. Pour rappel, la formule est la suivante:

(4/3) * pi * R³

avec R le rayon de la sphère (que vous demanderez à l'utilisateur de saisir).
Définissez pi = 3,14159
"""

PI = 3.14159

rayon = float(input("rayon : "))

volume = ((4 / 3) * PI) * rayon ** 3

print("rayon: ", rayon, ' volume:', round(volume, 2))
