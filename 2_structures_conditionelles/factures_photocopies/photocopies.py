"""
Un magasin de reprographie facture dégressivement en fonction du nombre de photocopies effectuées :

- 0,10 € / photocopie pour les 10 premières photocopies
- 0,09 € / photocopie les vingt suivantes
- 0,08 € / photocopie au-delà.

**Écrivez un algorithme qui demande à l’utilisateur le nombre de photocopies effectuées
et qui affiche la facture correspondante.**
"""

nb_photoc = int(input("Combien de photocopies avez-vous fait ? "))

if nb_photoc <= 10:
    total = 0.10 * nb_photoc

elif nb_photoc > 10 and nb_photoc <= 30:
    total = 0.10 * 10 + (nb_photoc-10) * 0.09

else:
    total = 0.10 * 10 + 0.09 * 20 + (nb_photoc - 30) * 0.08

print("Vous avez fait", nb_photoc, "photocopies, vous nous devez", total, "€")
print("Vous avez fait", nb_photoc, "photocopies, vous nous devez", round(total, 2), "€")



# Aller plus loin :
# Arrondir le résultat pour qu'il s'affiche avec 2 chiffres après la virgule
# print(round(total, 2))

# Simplifier le second test "si nb_photoc est compris entre X et Y"
