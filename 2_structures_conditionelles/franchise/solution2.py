"""
# Franchise

Une assurance effectue des remboursements en laissant une
franchise à la charge de l'assuré. Cette franchise représente
10 % du montant des dommages mais ne doit jamais dépasser 500 €.
Elle doit aussi être au minimum de 15 €.

- Ecrire un algorithme qui demande à l'utilisateur de saisir
le montant des dommages et lui affiche le montant remboursé,
puis le montant de la franchise (à la charge de l'assuré donc).
"""

montant_dmgs = int(input("Montant des dommages : "))

montant_dmgs = max(15, montant_dmgs)
franchise = 0.1 * montant_dmgs

franchise = max(15.0, franchise)
franchise = min(500.0, franchise)

remboursement = montant_dmgs - franchise
print("Vous serez remboursé de", remboursement,
      "avec une franchise de", franchise)
