"""
# Franchise

Une assurance effectue des remboursements en laissant une
franchise à la charge de l'assuré. Cette franchise représente
10 % du montant des dommages mais ne doit jamais dépasser 500 €.
Elle doit aussi être au minimum de 15 €.

- Ecrire un algorithme qui demande à l'utilisateur de saisir
le montant des dommages et lui affiche le montant remboursé,
puis le montant de la franchise (à la charge de l'assuré donc).
"""

montant_dmgs = int(input("Montant des dommages : "))

franchise = 0.1 * montant_dmgs

if montant_dmgs > 15:
    if franchise > 500:
        franchise = 500
    elif franchise < 15:
        franchise = 15

    remboursement = montant_dmgs - franchise

    print("Vous serez remboursé de", remboursement,
          "avec une franchise de", franchise)

else:
    print("Vous serez remboursé de 0 € car votre dommage est trop faible")

